#include "btscanner.h"
#include <iostream>
using namespace std;

BtScanner::BtScanner(QObject* obj) {
    discoveryAgent = new QBluetoothDeviceDiscoveryAgent(obj);
    server = new QBluetoothServer(QBluetoothServiceInfo::RfcommProtocol, obj);

    connect(discoveryAgent, SIGNAL(finished()), this, SLOT(finishedScan()));
    connect(server, SIGNAL(newConnection()), this, SLOT(clientConnected()));
    discoveryAgent->start();
}
void BtScanner::finishedScan() {
    for(auto device: discoveryAgent->discoveredDevices()) {
        cout << device.name().toStdString() << endl;
    }
}

void BtScanner::clientConnected() {
    QBluetoothSocket* connectionSocket = this->server->nextPendingConnection();
}
