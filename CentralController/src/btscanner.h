#ifndef BTSCANNER_H
#define BTSCANNER_H
#include <QObject>
#include <QtBluetooth>

class BtScanner: public QObject {
    Q_OBJECT
private:
    QBluetoothDeviceDiscoveryAgent* discoveryAgent;
    QBluetoothServer* server;
public:
    BtScanner(QObject* obj = 0);
public slots:
    void finishedScan();
    void clientConnected();
};

#endif // BTSCANNER_H
