#include "lock.h"
#include <iostream>
#include <QDataStream>
#include <QJsonDocument>
#include <QJsonObject>

using namespace Enigma;
using namespace std;

Lock::Lock(QString name, QObject* parent) {
    this->name = name;
    this->state = LockState::CLOSED;
}

Lock::Lock(QString name, QBluetoothAddress address, QObject *parent) : QObject(parent) {
    this->name = name;
    this->address = address;
    this->state = LockState::CLOSED;
}

LockState Lock::getLockState() const {
    return this->state;
}

QString Lock::getName() const {
    return this->name;
}

bool Lock::openLock() {
    if (this->state == LockState::CLOSED) {
        this->state = LockState::OPEN; // Send BT Open Command
        emit this->onLog("Lock has been opened", QDate());
        emit this->onLockChange(this->state);
        return true;
    } else {
        return false;
    }
}

bool Lock::closeLock() {
    if (this->state == LockState::OPEN) {
        this->state = LockState::CLOSED; // Send BT Close Command
        emit this->onLog("Lock has been closed", QDate());
        emit this->onLockChange(this->state);
        return true;
    } else {
        return false;
    }
}

QDataStream& Lock::operator <<(QDataStream& out) {
    out << this->address.toString() << this->state;
    return out;
}

QDataStream& Lock::operator >>(QDataStream& in) {
    QString address;
    int state;
    in >> address >> state;
    return in;
}

QJsonObject Lock::toJson() {
    QJsonObject json;
    json.insert("name", this->name);
    json.insert("state", this->state);
    return json;
}
