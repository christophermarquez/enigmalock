#ifndef LOCK_H
#define LOCK_H

#include <QObject>
#include <QBluetoothAddress>
#include <QDate>

namespace Enigma {
    enum LockState {
        CLOSED, OPEN
    };

    class Lock : public QObject {
        Q_OBJECT
        QBluetoothAddress address;
        LockState state;
        QString name;
    public:
        Lock(QString name, QObject* parent = 0);
        Lock(QString name, QBluetoothAddress address, QObject* parent = 0);
        LockState getLockState() const;
        bool openLock();
        bool closeLock();
        QString getName() const;
        QJsonObject toJson();
        QDataStream & operator <<(QDataStream & out);
        QDataStream & operator >>(QDataStream & in);
    signals:
        void onLockChange(LockState state);
        void onLog(QString log, QDate date);
    public slots:
    };
}

#endif // LOCK_H
