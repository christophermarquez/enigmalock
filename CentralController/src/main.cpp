#include <QCoreApplication>
#include <QSet>
#include <QSetIterator>
#include <QDataStream>
#include "btscanner.h"
#include "qrestfulserver.h"
#include "qhttpserverrequest.hpp"
#include "qhttpserverresponse.hpp"
#include <iostream>
#include "lock.h"

using namespace std;
using namespace qhttp::server;
using namespace Enigma;

void preflightRequest(QHttpRequest* req, QHttpResponse* res);

int main(int argc, char *argv[]) {

    QCoreApplication* app = new QCoreApplication(argc, argv);
    QRestfulServer* server = new QRestfulServer(app);
    QSet<Lock*> lockSet;
    Lock* lock1 = new Lock("Front Door");
    Lock* lock2 = new Lock("Bathroom");
    Lock* lock3 = new Lock("Main Room");
    lock2->openLock();
    lockSet.insert(lock1);
    lockSet.insert(lock2);
    lockSet.insert(lock3);

    server->get("/getAllLockNames", [&](QHttpRequest* req, QHttpResponse* res) {
        QSetIterator<Lock*> iterable(lockSet);
        QJsonObject json;
        QJsonArray array;

        while(iterable.hasNext()) {
            array.append(iterable.next()->getName());
        }

        json.insert("names", array);
        QJsonDocument jsonDoc(json);

        res->setStatusCode(qhttp::ESTATUS_OK);
        res->addHeader("Access-Control-Allow-Origin", "*");
        res->addHeader("Content-Type", "application/json");

        res->end(jsonDoc.toJson());
    });

    server->post("/changeLockState", [&](QHttpRequest* req, QHttpResponse* res) {
        req->onData([=](QByteArray data) {
            QJsonObject json = QJsonDocument::fromJson(data).object();
            QString lockName = json.value("name").toString();
            QSetIterator<Lock*> iterable(lockSet);

            while(iterable.hasNext()) {
                Lock* lock = iterable.next();
                if (lock->getName() == lockName) {
                   if (lock->getLockState()) {
                       cout << "Closing Lock " << lock->getName().toStdString() << endl;
                       lock->closeLock();
                   } else {
                       cout << "Opening Lock " << lock->getName().toStdString() << endl;
                       lock->openLock();
                   }

                   res->setStatusCode(qhttp::ESTATUS_OK);
                   res->addHeader("Access-Control-Allow-Origin", "*");
                   res->addHeader("Content-Type", "application/json");

                   QJsonObject json;
                   json.insert("result", lock->getLockState() ? true : false);
                   QJsonDocument jsonDoc(json);

                   res->end(jsonDoc.toJson());
                   return;
                }
            }
        });
    });

    server->get("/getAllLocks", [&](QHttpRequest* req, QHttpResponse* res) {
        QSetIterator<Lock*> iterable(lockSet);
        QJsonObject json;
        QJsonArray array;

        while(iterable.hasNext()) {
            Lock* lock = iterable.next();
            array.append(lock->toJson());
        }

        json.insert("locks", array);
        QJsonDocument jsonDoc(json);

        res->addHeader("Access-Control-Allow-Origin", "*");
        res->addHeader("Content-Type", "application/json");
        res->setStatusCode(qhttp::ESTATUS_OK);

        res->end(jsonDoc.toJson());
    });

    server->post("/getLock", [&](QHttpRequest* req, QHttpResponse* res) {
        cout << "Request Received" << endl;
        req->onData([=](QByteArray data) {
            cout << data.toStdString() << endl;

            QJsonObject json = QJsonDocument::fromJson(data).object();
            QString lockName = json.value("name").toString();
            QSetIterator<Lock*> iterable(lockSet);

            while(iterable.hasNext()) {
                Lock* lock = iterable.next();
                if (lock->getName() == lockName) {
                    QJsonDocument jsonDoc(lock->toJson());

                    res->setStatusCode(qhttp::ESTATUS_OK);
                    res->addHeader("Access-Control-Allow-Origin", "*");
                    res->addHeader("Access-Control-Allow-Methods", "*");
                    res->addHeader("Access-Control-Allow-Headers", "*");
                    res->addHeader("Content-Type", "application/json");

                    cout << "Sending response" << endl;

                    res->end(jsonDoc.toJson());
                    return;
                }
            }
            res->addHeader("Access-Control-Allow-Origin", "*");
            res->setStatusCode(qhttp::ESTATUS_NOT_FOUND);
            res->end();
            return;
        });
    });

    server->options("/getLock", preflightRequest);
    server->options("/changeLockState", preflightRequest);

    server->listen(QHostAddress::Any, 3000);

    return app->exec();
}

void preflightRequest(QHttpRequest* req, QHttpResponse* res) {
        cout << "Asking for preflight" << endl;
        res->setStatusCode(qhttp::ESTATUS_OK);
        res->addHeader("Access-Control-Allow-Origin", "*");
        res->addHeader("Access-Control-Allow-Headers", "Content-Type");
        res->end();
}
