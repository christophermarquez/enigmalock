#include "qrestfulserver.h"
#include "qhttpserverrequest.hpp"
#include "qhttpserverresponse.hpp"
#include <iostream>
using namespace std;
using namespace qhttp::server;

QRestfulServer::QRestfulServer(QCoreApplication* app): QHttpServer(app) {

}

bool QRestfulServer::listen(QHostAddress address, qint16 port, TServerHandler handler) {
    return QHttpServer::listen(address, port, [&](QHttpRequest* req, QHttpResponse* res) {
        QString httpVerb = qhttp::Stringify::toString(req->method());
        bool success = false;
        if (httpVerb == "GET") {
            success = findHandler(getRoutes, req, res);
        } else if (httpVerb == "POST") {
            success = findHandler(postRoutes, req, res);
        } else if (httpVerb == "OPTIONS") {
            success = findHandler(optionRoutes, req, res);
        }
        if (!success && handler != nullptr) { //No specialized listener, default listener
            handler(req, res);
        } else if (!success && handler == nullptr) { //No listener at all
            res->end("Not Implemented!");
        }
     });
}

bool QRestfulServer::findHandler(QList<Route*> handlerList, QHttpRequest* req, QHttpResponse* res) {
    Route* route = nullptr;
    for (int i = 0; i < handlerList.size(); i++) {
        if (handlerList.at(i)->getRoute() == req->url().url()) {
            route = handlerList.takeAt(i);
            break;
        }
    }
    if (route != nullptr) {
        handlerList << route;
        route->getHandler()(req, res);
        return true;
    } else {
        return false;
    }
}

void QRestfulServer::get(QString route, TServerHandler handler) {
    this->getRoutes << new Route(route, handler);
}

void QRestfulServer::post(QString route, TServerHandler handler) {
    this->postRoutes << new Route(route, handler);
}

void QRestfulServer::put(QString route, TServerHandler handler) {
    this->putRoutes << new Route(route, handler);
}

void QRestfulServer::options(QString route, TServerHandler handler) {
    this->optionRoutes << new Route(route, handler);
}

