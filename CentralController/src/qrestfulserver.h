#ifndef QRESTFULSERVER_H
#define QRESTFULSERVER_H
#include <QCoreApplication>
#include "route.h"
#include "qhttpserver.hpp"

using namespace qhttp::server;

class QRestfulServer: public QHttpServer {
private:
    QList<Route*> getRoutes;
    QList<Route*> postRoutes;
    QList<Route*> putRoutes;
    QList<Route*> optionRoutes;

    bool findHandler(QList<Route*> handlerList, QHttpRequest* req, QHttpResponse* res);
public:
    QRestfulServer(QCoreApplication* app);
    bool listen(QHostAddress address, qint16 port, TServerHandler handler = 0);
    void get(QString route, TServerHandler handler);
    void post(QString route, TServerHandler handler);
    void put(QString route, TServerHandler handler);
    void options(QString route, TServerHandler handler);
};

#endif // QRESTFULSERVER_H
