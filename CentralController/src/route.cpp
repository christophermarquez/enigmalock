#include "route.h"

Route::Route(QString route, TServerHandler handler) {
    this->route = route;
    this->handler = handler;
}

QString Route::getRoute() const {
    return this->route;
}

TServerHandler Route::getHandler() const {
    return this->handler;
}
