#ifndef ROUTE_H
#define ROUTE_H
#include "qhttpserver.hpp"
using namespace qhttp::server;
class Route {
private:
    QString route;
    TServerHandler handler;
public:
    Route(QString route, TServerHandler handler);
    QString getRoute() const;
    TServerHandler getHandler() const;
};

#endif // ROUTE_H
